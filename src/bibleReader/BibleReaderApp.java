package bibleReader;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import bibleReader.model.ArrayListBible;
import bibleReader.model.Bible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.VerseList;

/**
 * The main class for the Bible Reader Application.
 * 
 * @author cusack
 * @author Trevor Palmatier. Implemented 2/19/2020.
 */
public class BibleReaderApp extends JFrame {
	public static final int width = 800;
	public static final int height = 620;

	public static void main(String[] args) {
		new BibleReaderApp();
	}

	// Fields
	private BibleReaderModel model;
	private ResultView resultView;
	private JButton wordSearchButton;
	private JButton passageSearchButton;
	private JTextField searchInput;
	private JLabel searchLabel;
	private JMenuBar menuBar;
	private JMenu fileMenu;
	private JMenu helpMenu;
	private JMenuItem quitMenuItem;
	private JMenuItem aboutMenuItem;

	/**
	 * Default constructor. Prepares the GUI to be set up and then displays the set
	 * up GUI.
	 *
	 */
	public BibleReaderApp() {
		model = new BibleReaderModel();
		File kjvFile = new File("kjv.atv");
		VerseList verses = BibleIO.readBible(kjvFile);
		// File esvFile = new File("esv.atv");
		// VerseList esvVerses = BibleIO.readBible(esvFile);

		// Bible esv = new ArrayListBible(esvVerses);
		Bible kjv = new ArrayListBible(verses);

		model.addBible(kjv);
		// model.addBible(esv);

		resultView = new ResultView(model);

		setupGUI();
		pack();
		setSize(width, height);

		// So the application exits when you click the "x".
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	/**
	 * Set up the main GUI.
	 */
	private void setupGUI() {
		setTitle("Bible Reader");

		menuBar = new JMenuBar();
		menuBar.setBorderPainted(true);

		fileMenu = new JMenu("File");

		helpMenu = new JMenu("Help");

		quitMenuItem = new JMenuItem("Quit");
		quitMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		String bio = "Written by: Trevor Palmatier and Ryan DeWitt\nAn app that allows you to search the bible for passages or for phrases.";

		aboutMenuItem = new JMenuItem("About");
		aboutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, bio, "About", JOptionPane.INFORMATION_MESSAGE);
			}
		});

		fileMenu.add(quitMenuItem);
		helpMenu.add(aboutMenuItem);
		menuBar.add(fileMenu);
		menuBar.add(helpMenu);

		this.setJMenuBar(menuBar);

		// The items that belong to the search GUI panel.
		wordSearchButton = new JButton("Search Keyword");
		searchInput = new JTextField(30);
		searchLabel = new JLabel("Enter a Keyword or Passage:");

		passageSearchButton = new JButton("Search Passage");
		passageSearchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				resultView.doPassageSearch(searchInput.getText());
			}
		});

		ActionListener parseWordSearch = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				resultView.doWordSearch(searchInput.getText());
			}
		};
		wordSearchButton.addActionListener(parseWordSearch);
		// searchInput.addActionListener(parseWordSearch);

		// Placing everything when it should go on the main content pane.
		Container contentsMain = this.getContentPane();

		JPanel searchPanel = new JPanel(new FlowLayout());

		contentsMain.add(searchPanel, BorderLayout.NORTH);
		contentsMain.add(resultView, BorderLayout.CENTER);

		searchPanel.add(searchLabel);
		searchPanel.add(searchInput);
		searchPanel.add(wordSearchButton);
		searchPanel.add(passageSearchButton);

	}

}
